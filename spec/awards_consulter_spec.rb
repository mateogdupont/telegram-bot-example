require 'spec_helper'
require 'dotenv/load'
require 'web_mock'
require_relative '../app/awards_consulter'

describe AwardsConsulter do
  it 'AwardsConsulter can response its url' do
    api_key = 'fake_key'
    api_url = 'https://www.omdbapi.com/'
    consulter = described_class.new(api_url, api_key)
    expect(consulter.url).to eq api_url
  end

  it 'AwardsConsulter can response the awars of Titanic' do
    consulter = described_class.new('https://www.omdbapi.com/', 'fake_key')
    stub_request(:get, 'https://www.omdbapi.com/?apiKey=fake_key&t=Titanic')
      .with(headers: { 'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Faraday v2.7.4' })
      .to_return(status: 200, body: '{"Title":"Titanic","Awards":"Won 11 Oscars. 126 wins & 83 nominations total","Response": "True"}', headers: {})
    expect(consulter.get_awards_for_movie('Titanic')).to eq 'Won 11 Oscars. 126 wins & 83 nominations total'
  end

  it 'should respond with Received no nominations nor won any awards when is asked for Terminator' do
    consulter = described_class.new('https://www.omdbapi.com/', 'fake_key')
    stub_request(:get, 'https://www.omdbapi.com/?apiKey=fake_key&t=Terminator')
      .with(headers: { 'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Faraday v2.7.4' })
      .to_return(status: 200, body: '{"Title":"Terminator","Awards":"N/A","Response": "True"}', headers: {})
    expect(consulter.get_awards_for_movie('Terminator')).to eq 'Received no nominations nor won any awards'
  end

  it 'should respond "Movie not found!" when querying for a nonexistentmovie' do
    consulter = described_class.new('https://www.omdbapi.com/', 'fake_key')
    stub_request(:get, 'https://www.omdbapi.com/?apiKey=fake_key&t=nonexistentmovie')
      .with(headers: { 'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Faraday v2.7.4' })
      .to_return(status: 200, body: '{"Response": "False","Error": "Movie not found!"}', headers: {})
    expect(consulter.get_awards_for_movie('nonexistentmovie')).to eq 'Movie not found!'
  end
end
