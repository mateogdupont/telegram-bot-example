require 'spec_helper'
require_relative '../app/tv/movie'
require 'web_mock'

def stub_award_request
  api_omdb_response_body = {
    "Title": 'Titanic',
    "Awards": 'Won 11 Oscars. 126 wins & 83 nominations total',
    "Response": 'True'
  }
  stub_request(:get, 'https://www.omdbapi.com/?apiKey=fake_key&t=Titanic')
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: api_omdb_response_body.to_json, headers: {})
end

def stub_award_request_for_nonexistentmovie
  api_omdb_response_body = {
    "Response": 'False',
    "Error": 'Movie not found!'
  }
  stub_request(:get, 'https://www.omdbapi.com/?apiKey=fake_key&t=nonexistentmovie')
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: api_omdb_response_body.to_json, headers: {})
end

describe Movie do
  it 'should response its title' do
    awards_consulter = AwardsConsulter.new('fake_url', 'fake_key')
    movie = described_class.new('Titanic', awards_consulter)
    expect(movie.title).to eq 'Titanic'
  end

  it 'should response with its awards' do
    awards_consulter = AwardsConsulter.new('https://www.omdbapi.com/', 'fake_key')
    movie = described_class.new('Titanic', awards_consulter)
    stub_award_request
    expect(movie.calculate_awards).to eq 'Won 11 Oscars. 126 wins & 83 nominations total'
  end

  it 'should response with Cant get the awards for this movie when consultant fail to calculate the awards' do
    awards_consulter = AwardsConsulter.new('https://www.omdbapi.com/', 'fake_key')
    movie = described_class.new('nonexistentmovie', awards_consulter)
    stub_award_request_for_nonexistentmovie
    expect(movie.calculate_awards).to eq 'Cant get the awards for this movie'
  end
end
