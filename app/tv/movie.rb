class Movie
  attr_reader :title

  ERROR_MESSAGE = 'Movie not found!'.freeze
  MESSAGE_OF_CONSULTANT_ERROR = 'Cant get the awards for this movie'.freeze

  def initialize(title, awards_consulter)
    @title = title
    @consulter = awards_consulter
  end

  def calculate_awards
    response = @consulter.get_awards_for_movie(@title)
    return MESSAGE_OF_CONSULTANT_ERROR if response == ERROR_MESSAGE

    response
  end
end
