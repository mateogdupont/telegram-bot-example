require 'faraday'

class AwardsConsulter
  MESSAGE_IN_CASE_OF_NO_AWARDS = 'Received no nominations nor won any awards'.freeze
  NOT_APPLY_MESSAGE = 'N/A'.freeze
  NOT_RESPONSE = 'False'.freeze
  attr_reader :url

  def initialize(url, key)
    @url = url
    @key = key
  end

  def get_awards_for_movie(title)
    response = Faraday.get(@url, { t: title, apiKey: @key })
    response_json = JSON.parse(response.body)
    return response_json['Error'] if response_json['Response'] == NOT_RESPONSE

    if response_json['Awards'] == NOT_APPLY_MESSAGE
      MESSAGE_IN_CASE_OF_NO_AWARDS
    else
      response_json['Awards']
    end
  end
end
